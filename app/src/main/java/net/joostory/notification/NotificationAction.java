package net.joostory.notification;

import android.app.PendingIntent;

public class NotificationAction {

	private int icon;
	private CharSequence title;
	private PendingIntent intent;
	
	public NotificationAction(int icon, String title, PendingIntent intent) {
		this.icon = icon;
		this.title = title;
		this.intent = intent;
	}
	
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public CharSequence getTitle() {
		return title;
	}
	public void setTitle(CharSequence title) {
		this.title = title;
	}
	public PendingIntent getIntent() {
		return intent;
	}
	public void setIntent(PendingIntent intent) {
		this.intent = intent;
	}
	
}
