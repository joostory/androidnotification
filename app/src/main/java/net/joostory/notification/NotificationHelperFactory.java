package net.joostory.notification;

import android.os.Build;

public class NotificationHelperFactory {

	private static NotificationHelper instance = null;
	private NotificationHelperFactory() {}

	public static NotificationHelper create() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			return createOldHelper();
		} else {
			return createNewHelper();
		}
	}
	
	public static NotificationHelper createOldHelper() {
		if (instance == null || !(instance instanceof OldNotificationHelper)) {
			instance = new OldNotificationHelper();
		}
		return instance;
	}

	public static NotificationHelper createNewHelper() {
		if (instance == null || !(instance instanceof NewNotificationHelper)) {
			instance = new NewNotificationHelper();
		}
		return instance;
	}
	
}
