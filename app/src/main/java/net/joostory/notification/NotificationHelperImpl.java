package net.joostory.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

public abstract class NotificationHelperImpl implements NotificationHelper {

    NotificationManager notificationManager;

    protected NotificationManager getNotificationManager(Context context) {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

	@Override
    public void notifyOnGoing(Context context, int notificationId, NotificationItem notificationItem) {
        notify(context, notificationId, notificationItem, Notification.FLAG_ONGOING_EVENT);
    }

	@Override
	public void notify(Context context, int notificationId, NotificationItem notificationItem) {
		notify(context, notificationId, notificationItem, Notification.FLAG_AUTO_CANCEL);		
	}

	@Override
	public abstract void notify(Context context, int notificationId, NotificationItem notificationItem, int flags);

	@Override
	public void cancel(Context context, int notificationId) {
        getNotificationManager(context).cancel(notificationId);
	}

}
