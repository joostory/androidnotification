package net.joostory.notification;

import android.content.Context;

public interface NotificationHelper {
	
	public void notifyOnGoing(Context context, int notificationId, NotificationItem notificationItem);

	public void notify(Context context, int notificationId, NotificationItem notificationItem);
	
	public void notify(Context context, int notificationId, NotificationItem notificationItem, int flags);
	
	public void cancel(Context context, int notificationId);
}
