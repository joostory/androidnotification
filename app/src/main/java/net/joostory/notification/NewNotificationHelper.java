package net.joostory.notification;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.Builder;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;

@SuppressLint("NewApi")
public class NewNotificationHelper extends NotificationHelperImpl {

	@SuppressWarnings("deprecation")
	@Override
	public void notify(Context context, int notificationId, NotificationItem notificationItem, int flags) {
		Notification noti = null;
		Builder builder = makeDefaultBuilder(context, notificationItem);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			noti = builder.getNotification();
		} else {
			noti = builder.build();
		}

		noti.flags |= flags;
		getNotificationManager(context).notify(notificationId, noti);
	}

	
	private static Builder makeDefaultBuilder(Context context, NotificationItem notificationItem) {
		Builder builder = new Notification.Builder(context);
		builder.setContentTitle(notificationItem.getContentTitle());
		builder.setContentText(notificationItem.getContentText());
		builder.setTicker(notificationItem.getTicker());
		builder.setWhen(notificationItem.getWhen());
		builder.setContentIntent(notificationItem.getContentIntent());
		builder.setDefaults(Notification.DEFAULT_LIGHTS);

		if (notificationItem.getSound() != null) {
			builder.setSound(notificationItem.getSound());
		}
		if (notificationItem.isVibrate()) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && notificationItem.getStyle() != null) {
			builder.setStyle(notificationItem.getStyle());
		}

        if (notificationItem.getLargeIcon() != null) {
            builder.setLargeIcon(notificationItem.getLargeIcon());
        } else if (notificationItem.getLargeIconResourceId() != 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), notificationItem.getLargeIconResourceId()));
        }

		if (notificationItem.getSmallIconResourceId() != 0) {
			builder.setSmallIcon(notificationItem.getSmallIconResourceId());
		}

        builder.setNumber(notificationItem.getNumber());
        if (notificationItem.getContentInfo() != null) {
            builder.setContentInfo(notificationItem.getContentInfo());
        }

		if (notificationItem.getDeleteIntent() != null) {
            builder.setDeleteIntent(notificationItem.getDeleteIntent());
        }

        if (notificationItem.getMaxProgress() != 0) {
            builder.setProgress(notificationItem.getMaxProgress(), notificationItem.getProgress(), notificationItem.getProgress() == 0);
        }

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			List<NotificationAction> actions = notificationItem.getActions();
			if (actions != null) {
				for(NotificationAction action : actions) {
					builder.addAction(action.getIcon(), action.getTitle(), action.getIntent());
				}
			}
		}

		return builder;
	}

}
