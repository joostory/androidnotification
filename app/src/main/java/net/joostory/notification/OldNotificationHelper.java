package net.joostory.notification;

import android.app.Notification;
import android.content.Context;

public class OldNotificationHelper extends NotificationHelperImpl {

	@SuppressWarnings("deprecation")
	@Override
	public void notify(Context context, int notificationId, NotificationItem notificationItem, int flags) {
		Notification notification = new Notification(notificationItem.getLargeIconResourceId(),
				notificationItem.getTicker(), notificationItem.getWhen());
		notification.setLatestEventInfo(context, notificationItem.getContentTitle(), notificationItem.getContentText(),
				notificationItem.getContentIntent());
		notification.defaults |= Notification.DEFAULT_LIGHTS;

		if (notificationItem.getSound() != null) {
			notification.sound = notificationItem.getSound();
		}
		if (notificationItem.isVibrate()) {
			notification.defaults |= Notification.DEFAULT_VIBRATE;
		}
		if (notificationItem.getDeleteIntent() != null) {
			notification.deleteIntent = notificationItem.getDeleteIntent();
		}

		notification.when = notificationItem.getWhen();
		notification.flags |= flags;

		getNotificationManager(context).notify(notificationId, notification);
	}

}
