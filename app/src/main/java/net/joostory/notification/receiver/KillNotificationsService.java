package net.joostory.notification.receiver;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import net.joostory.notification.NotificationHelperFactory;

public class KillNotificationsService extends Service {

    public class KillBinder extends Binder {
        public final Service service;

        public KillBinder(Service service) {
            this.service = service;
        }

    }

    public static int NOTIFICATION_ID = 6;
    private NotificationManager mNM;
    private final IBinder mBinder = new KillBinder(this);

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("DEBUG", "KillNotificationsService.onBind");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("DEBUG", "KillNotificationsService.onStartCommand");
        if (intent != null) {
            int notiId = intent.getIntExtra("notiId", 0);
            Log.d("DEBUG", "KillNotificationsService.onStartCommand notiId : " + notiId);
            NotificationHelperFactory.create().cancel(getApplicationContext(), notiId);
        } else {
            Log.d("DEBUG", "KillNotificationsService.onStartCommand intent null");
        }
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onCreate() {
        Log.d("DEBUG", "KillNotificationsService.onCreate");
    }
}
