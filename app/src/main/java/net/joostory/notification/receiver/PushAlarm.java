package net.joostory.notification.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import net.joostory.notification.NotificationHelperFactory;
import net.joostory.notification.activity.NotificationHelperTest;

public class PushAlarm extends BroadcastReceiver {

	private static final int REQUEST_CODE = 22681004;
	private static final long PUSH_INTERVAL = 1000L;
	public static final String SERVICE_PUSH = "net.joostory.mailnotification.service.PUSH";
	public static final String SERVICE_DELETE_PUSH = "net.joostory.mailnotification.service.DELETE_PUSH";
    public static final String NOTIFICATION_CALL = "net.joostory.mailnotification.service.NOTIFICATION_CALL";
    public static final String NOTIFICATION_DELETE = "net.joostory.mailnotification.service.NOTIFICATION_DELETE";
    public static final String NOTIFICATION_EDIT = "net.joostory.mailnotification.service.NOTIFICATION_EDIT";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
        Bundle extras = intent.getExtras();
		Log.d("DEBUG", "action : " + action);
		
		if (action == null) {
			return;
		}
		
		if (SERVICE_PUSH.equals(action)) {
			NotificationHelperTest.showOldNotification(context);
			unset(context);
		} else if (SERVICE_DELETE_PUSH.equals(action)) {
            String message = extras.getString("message");
			Log.d("Push", action + " : " + message);
		} else if (NOTIFICATION_CALL.equals(action)) {
            int notificationId = extras.getInt("notificationId", 0);
            if (notificationId != 0) {
                NotificationHelperFactory.create().cancel(context, notificationId);
            }
        } else if (NOTIFICATION_DELETE.equals(action)) {
            int notificationId = extras.getInt("notificationId", 0);
            if (notificationId != 0) {
                NotificationHelperFactory.create().cancel(context, notificationId);
            }
        } else if (NOTIFICATION_EDIT.equals(action)) {
            int notificationId = extras.getInt("notificationId", 0);
            if (notificationId != 0) {
                NotificationHelperFactory.create().cancel(context, notificationId);
            }
        }
	}
	
	public void set(Context context) {
		Log.d("Push", "set PushAlarm");

		Intent intent = new Intent(context, PushAlarm.class);
		intent.setAction(SERVICE_PUSH);
		PendingIntent operation = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarmManager=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + PUSH_INTERVAL, PUSH_INTERVAL, operation);
	}
	
	public void unset(Context context) {
		Log.d("Push", "unset PushAlarm");
		
		Intent intent = new Intent(context, PushAlarm.class);
		intent.setAction(SERVICE_PUSH);
		PendingIntent operation = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(operation);
	}
	
}
