package net.joostory.notification.activity;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Notification.InboxStyle;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.widget.RemoteViews;

import net.joostory.notification.NotificationHelperFactory;
import net.joostory.notification.NotificationAction;
import net.joostory.notification.NotificationItem;
import net.joostory.notification.R;
import net.joostory.notification.receiver.PushAlarm;
import net.joostory.notification.utils.FileUtils;

public class NotificationHelperTest {
	
	public static void showTicker(Context context) {

        NotificationItem item = new NotificationItem();
        item.setTicker("Ticker Only");
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setVibrate(false);

		NotificationHelperFactory.create().notify(context, 4, item);
        NotificationHelperFactory.create().cancel(context, 4);
	}
	
	@SuppressLint("NewApi")
	public static void showNewNotification(final Context context) {
		
		final int contentId = 3;

        NotificationItem item = new NotificationItem();
        item.setContentTitle("New mail from " + "test@gmail.com");
        item.setContentText("ContentText");
        item.setTicker("Ticker");
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setNumber(99);
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setVibrate(true);
        item.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0));
        item.setDeleteIntent(makeDeleteNotification(context, contentId, "showNewNotification"));

		NotificationHelperFactory.create().notify(context, contentId, item);
	}

	@SuppressLint("NewApi")
	public static void showNewBigNotification(final Context context) {
		
		final int contentId = 2;

        NotificationItem item = new NotificationItem();
        item.setContentTitle("BigContentTitle");
        item.setContentText("ContentText");
        item.setTicker("Big Ticker!");
        item.setStyle(new Notification.BigTextStyle()
                .bigText("fskjdfjkslkfjklsdf slkdfjslkjflskjd sdlfjsldjf slkdfjsldkfjsd sldfjsldjkfs slkdjflskjdlfjksldjf slkdjflskjdksdf")
                .setBigContentTitle("BigContentTitle")
                .setSummaryText("SummaryText"));
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setWhen(System.currentTimeMillis());
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0));
        item.setDeleteIntent(makeDeleteNotification(context, contentId, "showNewBigNotification"));
		NotificationHelperFactory.create().notify(context, contentId, item);
	}
	
	@SuppressLint("NewApi")
	public static void showInboxNotification(Context context) {
		
		int contentId = 4;
		
		Intent intent = new Intent(context, MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		InboxStyle style = new Notification.InboxStyle();
		style.setBigContentTitle("InboxStyle BigContentTitle")
			.setSummaryText("SummaryText");
		for(int i = 0 ; i < 10 ; i++) {
			SpannableString str = new SpannableString("TEST (" + i + ")HAHAHA");
			str.setSpan(new StyleSpan(Typeface.BOLD), 0, 4, 0);
			str.setSpan(new ForegroundColorSpan(Color.CYAN), 0, 4, 0);
			style.addLine(str);
		}

        NotificationItem item = new NotificationItem();
        item.setContentTitle("InboxStyle ContentTitle");
		item.setContentText("ContentText");
        item.setTicker("InboxStyle Ticker!");
        item.setContentIntent(pIntent);
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setStyle(style);
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setNumber(10);
        item.setDeleteIntent(makeDeleteNotification(context, contentId, "showInboxNotification"));

        NotificationHelperFactory.create().notify(context, contentId, item);
	}
	
	@SuppressLint("NewApi")
	public static void updateInboxNotification(Context context) {
		
		int contentId = 4;
		
		Intent intent = new Intent(context, MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		InboxStyle style = new Notification.InboxStyle();
		style.setBigContentTitle("InboxStyle BigContentTitle")
			.setSummaryText("SummaryText");
		for(int i = 0 ; i < 3 ; i++) {
			SpannableString str = new SpannableString("TEST (" + i + ")HAHAHA");
			str.setSpan(new StyleSpan(Typeface.BOLD), 0, 4, 0);
			str.setSpan(new ForegroundColorSpan(Color.CYAN), 0, 4, 0);
			style.addLine(str);
		}

        NotificationItem item = new NotificationItem();
		item.setContentTitle("InboxStyle ContentTitle");
        item.setContentText("ContentText");
        item.setTicker("InboxStyle Ticker!");
        item.setContentIntent(pIntent);
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setStyle(style);
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setNumber(10);
        item.setDeleteIntent(makeDeleteNotification(context, contentId, "updateInboxNotification"));

        NotificationHelperFactory.create().notify(context, contentId, item);
	}
	
	@SuppressWarnings("deprecation")
	public static void showOldNotification(Context context) {
		
		int contentId = 1;
		
        NotificationItem item = new NotificationItem();
        item.setContentTitle("Content Title");
        item.setContentText("Content text");
        item.setTicker("Notification text that will be shown on status bar.");
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setWhen(System.currentTimeMillis());
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0));
        item.setDeleteIntent(makeDeleteNotification(context, contentId, "showOldNotification"));

        NotificationHelperFactory.createOldHelper().notify(context, contentId, item);
	}
	
	private static PendingIntent makeDeleteNotification(Context context, int contentId, String message) {
		Intent intent = new Intent(context, PushAlarm.class);
		intent.setAction(PushAlarm.SERVICE_DELETE_PUSH);
		intent.putExtra("message", message);
		return PendingIntent.getBroadcast(context, contentId, intent, PendingIntent.FLAG_ONE_SHOT);
	}

    public static void showFileopenNotification(final Context context) {

        String filename = "sample.mov";

        final NotificationItem item = new NotificationItem();
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setWhen(System.currentTimeMillis());
        item.setTicker("Click and open File " + filename);
        item.setContentTitle("Click and open");
        item.setContentText(filename);
        item.setContentIntent(makeOpenFolderIntent(context, 6));
        item.addAction(new NotificationAction(android.R.drawable.ic_menu_call, "OpenFolder", makeOpenFolderIntent(context, 6)));
        NotificationHelperFactory.create().notify(context, 6, item);
    }

    private static PendingIntent makeOpenFolderIntent(Context context, int id) {
        Intent intent = getFileExplororIntent(context);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File("/sdcard/Download/")), "*/*");
        return PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private static final String[] FILE_EXPLORERS = {
            "com.estrongs.android.pop"
    };

    public static Intent getFileExplororIntent(Context context) {
        Intent intent = null;
        PackageManager packageManager = context.getPackageManager();
        for (String packageName : FILE_EXPLORERS) {
            intent = packageManager.getLaunchIntentForPackage(packageName);
            if (intent != null) {
                break;
            }
        }

        if (intent == null) {
            intent = new Intent();
        }
        return intent;
    }

	@SuppressWarnings("deprecation")
	public static void showOldProgressNotification(final Context context, final String filename) {


        final NotificationItem item = new NotificationItem();
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setTicker("Progress");
        item.setWhen(System.currentTimeMillis());
        item.setContentTitle("Old Download " + filename);
        item.setContentText("Prepare");
        item.setContentIntent(makePendingIntent(context));

        NotificationHelperFactory.createOldHelper().notifyOnGoing(context, 5, item);

		new Thread(
			new Runnable() {

				@Override
				public void run() {
					int incr;
					try {
						Thread.sleep(3*1000);
					} catch (InterruptedException e) {
						Log.d("Debug", "sleep failure");
					}
					for(incr = 0 ; incr <= 100 ; incr+=5) {
						
                        item.setContentText(incr + " %");
                        NotificationHelperFactory.createOldHelper().notifyOnGoing(context, 5, item);
						
						try {
							Thread.sleep(1*1000);
						} catch (InterruptedException e) {
							Log.d("Debug", "sleep failure");
						}
					}

                    item.setContentText("Complete");
                    NotificationHelperFactory.createOldHelper().notify(context, 5, item);
				}
				
			}
		).start();
	}
	
	@SuppressLint("NewApi")
	public static void showProgressNotification(final Context context, final String filename) {

        final NotificationItem item = new NotificationItem();
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setTicker("New Download " + filename);
        item.setContentTitle("New Download " + filename);
        item.setContentText("0 / 100 MB");
        item.setContentInfo("0%");
        item.setProgress(0);
        item.setMaxProgress(100);
        NotificationHelperFactory.create().notifyOnGoing(context, 6, item);

		new Thread(
			new Runnable() {

				@Override
				public void run() {
					try {
						Thread.sleep(3*1000);
					} catch (InterruptedException e) {
						Log.d("Debug", "sleep failure");
					}
					
					int incr;
					for(incr = 0 ; incr <= 100; incr+= 5) {
                        item.setProgress(incr);
                        item.setContentInfo(incr + "%");
                        item.setContentText(incr + " / 100MB");
                        NotificationHelperFactory.create().notifyOnGoing(context, 6, item);

						try {
							Thread.sleep(1*1000);
						} catch (InterruptedException e) {
							Log.d("Debug", "sleep failure");
						}
					}

                    item.setContentIntent(makePendingIntent(context));
                    item.setWhen(System.currentTimeMillis());
                    item.setMaxProgress(0);
                    item.setTicker("Download complete");
                    item.setContentText("Download complete");
                    item.setContentInfo(null);
                    NotificationHelperFactory.create().notify(context, 6, item);
				}
				
			}
		).start();
	}
	
	private static PendingIntent makePendingIntent(Context context) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
        String contentType = FileUtils.guessContentType("sample.mov");
        intent.setDataAndType(Uri.fromFile(new File("/sdcard/Download/sample.mov")), contentType);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public static void showDesignedNotification(Context context, String filename) {
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher, "Download " + filename, System.currentTimeMillis());
		RemoteViews progressView = new RemoteViews(context.getPackageName(), R.layout.progress_notification);
		progressView.setImageViewResource(R.id.progress_icon, R.drawable.ic_launcher);
		progressView.setTextViewText(R.id.progress_content, "0 / 100");
		progressView.setTextViewText(R.id.progress_number, "0 %");
		progressView.setTextViewText(R.id.progress_title, filename);
		
		notification.contentView = progressView;
		notification.contentView.setProgressBar(R.id.progress_status, 100, 0, true);
		notification.contentIntent = makePendingIntent(context);
		int notificationId = 7;
		manager.notify(notificationId, notification);
		
		try {
			Thread.sleep(1000);
			
			for (int i = 0 ; i <= 100 ; i+=10) {
				Thread.sleep(1000);
				progressView.setTextViewText(R.id.progress_content, i + " / 100");
				progressView.setTextViewText(R.id.progress_number, i + " %");
				progressView.setProgressBar(R.id.progress_status, 100, i, false);
				manager.notify(notificationId, notification);		
			}
			
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// do nothing
		}
		
		progressView.setTextViewText(R.id.progress_content, "Download Completed.");
		progressView.setTextViewText(R.id.progress_number, "100 %");
		notification.contentView.setProgressBar(R.id.progress_status, 100, 100, false);
		manager.notify(notificationId, notification);
	}

    @SuppressLint("NewApi")
    public static void showNotificationAction(Context context) {
        final int contentId = 8;

        NotificationItem item = new NotificationItem();
        item.setContentTitle("Notification Action");
        item.setContentText("Do something please");
        item.setTicker("Do something");
        item.setStyle(new Notification.BigTextStyle()
                .bigText("안드로이드 에뮬레이터로 테스트를 하면서 4.2 (API 17) 이상에서 한글이 나오지 않음을 알았습니다. 그간 뭘 잘못설치해서 한글이 안나오는 줄 알고 기계 연결을 해서 테스트를 했었는데 이미지에 한글 폰트가 없어서 그런거랍니다.")
                .setBigContentTitle("Notification Action")
                .setSummaryText("Do something please"));
        item.setLargeIconResourceId(R.drawable.ic_launcher);
        item.setSmallIconResourceId(R.drawable.ic_launcher);
        item.setNumber(99);
        item.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        item.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0));
        item.setDeleteIntent(makeDeleteNotification(context, contentId, "showNewNotification"));
        item.addAction(new NotificationAction(android.R.drawable.ic_menu_call, "Call", makeSampleActionIntent(context, PushAlarm.NOTIFICATION_CALL)));
        item.addAction(new NotificationAction(android.R.drawable.ic_menu_delete, "Delete", makeSampleActionIntent(context, PushAlarm.NOTIFICATION_DELETE)));
        item.addAction(new NotificationAction(android.R.drawable.ic_menu_edit, "Edit", makeSampleActionIntent(context, PushAlarm.NOTIFICATION_EDIT)));

        NotificationHelperFactory.create().notify(context, contentId, item);
    }

    private static PendingIntent makeSampleActionIntent(Context context, String action) {
        Intent intent = new Intent(context, PushAlarm.class);
        intent.setAction(action);
        intent.putExtra("notificationId", 8);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    }
}
