package net.joostory.notification.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import net.joostory.notification.receiver.AppMessage;
import net.joostory.notification.receiver.KillNotificationsService;
import net.joostory.notification.receiver.PushAlarm;
import net.joostory.notification.R;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		findViewById(R.id.show_ticker).setOnClickListener(this);
		findViewById(R.id.show_old_noti).setOnClickListener(this);
		findViewById(R.id.show_new_noti).setOnClickListener(this);
		findViewById(R.id.show_new_big_noti).setOnClickListener(this);
		findViewById(R.id.show_inbox_noti).setOnClickListener(this);
		findViewById(R.id.update_inbox_noti).setOnClickListener(this);
        findViewById(R.id.show_notification_action).setOnClickListener(this);
        findViewById(R.id.show_fileopen_noti).setOnClickListener(this);
		findViewById(R.id.show_old_progress_noti).setOnClickListener(this);
		findViewById(R.id.show_progress_noti).setOnClickListener(this);
		findViewById(R.id.show_designed_progress_noti).setOnClickListener(this);
		findViewById(R.id.btn_show_image_button_progress_noti).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onClick(final View v) {
		ServiceConnection mConnection = new ServiceConnection() {
		    public void onServiceDisconnected(ComponentName className) {
		    }

			@Override
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Intent intent = new Intent(MainActivity.this, KillNotificationsService.class);
				intent.putExtra("notiId", v.getId());
				((KillNotificationsService.KillBinder) binder).service.startService(intent);
				doAction(v);				
			}
		};
		bindService(new Intent(getApplicationContext(), KillNotificationsService.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	private void doAction(View v) {
		switch(v.getId()) {
		case R.id.show_ticker:
			NotificationHelperTest.showTicker(this);
			break;
		case R.id.show_old_noti:
			NotificationHelperTest.showOldNotification(this);
			break;
		case R.id.show_new_noti:
			NotificationHelperTest.showNewNotification(this);
			break;
		case R.id.show_new_big_noti:
			NotificationHelperTest.showNewBigNotification(this);
			break;
		case R.id.show_inbox_noti:
			NotificationHelperTest.showInboxNotification(this);
			break;
		case R.id.update_inbox_noti:
			NotificationHelperTest.updateInboxNotification(this);
			break;
        case R.id.show_fileopen_noti:
            NotificationHelperTest.showFileopenNotification(this);
            break;
		case R.id.show_old_progress_noti:
			NotificationHelperTest.showOldProgressNotification(this, "file.txt");
			break;
		case R.id.show_progress_noti:
			NotificationHelperTest.showProgressNotification(this, "file.txt");
			break;
		case R.id.show_designed_progress_noti:
			NotificationHelperTest.showDesignedNotification(this, "file.txt");
			break;
		case R.id.btn_show_image_button_progress_noti:
			showNotificationWithImageButton();
			break;
        case R.id.show_notification_action:
            NotificationHelperTest.showNotificationAction(this);
            break;
		}
	}
	
	private void showNotificationWithImageButton() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				mHandler.sendMessage(Message.obtain(null, AppMessage.START_DOWNLOAD));
				NotificationHelperTest.showDesignedNotification(MainActivity.this, "very_very_very_very_very_very_very_very_long_name.txt");
				mHandler.sendMessage(Message.obtain(null, AppMessage.FINISH_DOWNLOAD));
			}
		}).start();
	}
	
	private Handler mHandler = new Handler() {
		@Override
	    public void handleMessage(Message msg) {
			switch(msg.what) {
			case AppMessage.FINISH_DOWNLOAD:
				finishDownload();
				break;
			case AppMessage.START_DOWNLOAD:
				startDownload();
				break;
			default:
				break;
			}
		}
	};
	
	private void startDownload() {
		ImageButton imgBtn = (ImageButton) findViewById(R.id.btn_show_image_button_progress_noti);
		imgBtn.setImageResource(R.drawable.img_loading);
		imgBtn.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotation));
		imgBtn.setOnClickListener(null);
	}
	
	private void finishDownload() {
		ImageButton imgBtn = (ImageButton) findViewById(R.id.btn_show_image_button_progress_noti);
		imgBtn.setImageResource(R.drawable.selector_btn_image_button_progress_noti);
		imgBtn.clearAnimation();
		imgBtn.setOnClickListener(this);
	}
	
}
