package net.joostory.notification;

import java.util.ArrayList;
import java.util.List;

import android.app.PendingIntent;
import android.app.Notification.Style;
import android.graphics.Bitmap;
import android.net.Uri;

public class NotificationItem {
	
	private CharSequence contentTitle;
    private CharSequence contentText;
    private CharSequence subText;
    private CharSequence contentInfo;
    private int number;
    private long when;

    private CharSequence ticker;
    private Bitmap largeIcon;
    private int largeIconResourceId;

	private int smallIconResourceId;
    private Uri sound;
    private boolean vibrate;

    private Style style;
	private List<NotificationAction> actions = new ArrayList<NotificationAction>();
    private int progress;
    private int maxProgress;

    private PendingIntent contentIntent;
    private PendingIntent deleteIntent;


    public CharSequence getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(CharSequence contentTitle) {
        this.contentTitle = contentTitle;
    }

    public CharSequence getContentText() {
        return contentText;
    }

    public void setContentText(CharSequence contentText) {
        this.contentText = contentText;
    }

    public CharSequence getSubText() {
        return subText;
    }

    public void setSubText(CharSequence subText) {
        this.subText = subText;
    }

    public CharSequence getContentInfo() {
        return contentInfo;
    }

    public void setContentInfo(CharSequence contentInfo) {
        this.contentInfo = contentInfo;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }

    public CharSequence getTicker() {
        return ticker;
    }

    public void setTicker(CharSequence ticker) {
        this.ticker = ticker;
    }

    public Bitmap getLargeIcon() {
        return largeIcon;
    }

    public void setLargeIcon(Bitmap largeIcon) {
        this.largeIcon = largeIcon;
    }

    public int getLargeIconResourceId() {
        return largeIconResourceId;
    }

    public void setLargeIconResourceId(int largeIconResourceId) {
        this.largeIconResourceId = largeIconResourceId;
    }

    public int getSmallIconResourceId() {
        return smallIconResourceId;
    }

    public void setSmallIconResourceId(int smallIconResourceId) {
        this.smallIconResourceId = smallIconResourceId;
    }

    public Uri getSound() {
        return sound;
    }

    public void setSound(Uri sound) {
        this.sound = sound;
    }

    public boolean isVibrate() {
        return vibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.vibrate = vibrate;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public List<NotificationAction> getActions() {
        return actions;
    }

    public void setActions(List<NotificationAction> actions) {
        this.actions = actions;
    }

    public void addAction(NotificationAction action) {
        if (actions == null) {
            actions = new ArrayList<NotificationAction>();
        }
        actions.add(action);
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getMaxProgress() {
        return maxProgress;
    }

    public void setMaxProgress(int maxProgress) {
        this.maxProgress = maxProgress;
    }

    public PendingIntent getContentIntent() {
        return contentIntent;
    }

    public void setContentIntent(PendingIntent contentIntent) {
        this.contentIntent = contentIntent;
    }

    public PendingIntent getDeleteIntent() {
        return deleteIntent;
    }

    public void setDeleteIntent(PendingIntent deleteIntent) {
        this.deleteIntent = deleteIntent;
    }

}